//Gustav Masch
//CSE002 
//LAB02: Cyclometer
//The purpose of the lab is to create a program that
//calculates the distance covered by a bicycle using the amount of 
//counts of rotations from the wheel
public class Cyclometer {
  //main mehtod required for every Java program
  public static void main(String[] args) {
    int secsTrip1=480;  
    //define the amount of seconds in trip1
    double secsTrip2=3220;  
    //define the amount of seconds in trip2
		int countsTrip1=1561;  
    //define amount of counts in trip1
		int countsTrip2=9037; 
    //define amount of counts in trip2
    double wheelDiameter=27.0;  
    //define wheel diameter
  	double PI=3.14159; 
    //define value of pi
  	int feetPerMile=5280; 
    //define feet to miles conversion factor
  	int inchesPerFoot=12;   
    //define inches to feet conversion factor
  	int secondsPerMinute=60;  
    //define seconds to minutes conversion factor
	  double distanceTrip1, distanceTrip2,totalDistance;
    //state the data type of the results
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
    //Print the time trip 1 took
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
    //Print the time trip 2 took
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //calculate distance of trip 1 based on wheel turning counts 
	  distanceTrip1/=inchesPerFoot*feetPerMile; 
    //convert the result of trip 1 into miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    //calculate the distance trip 2 took based on wheel turning counts
	  totalDistance=distanceTrip1+distanceTrip2;
    //calculate the total distance of both trips
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    //Print the distance covered in trip 1
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
    //Print the distance covered in trip 2
	  System.out.println("The total distance was "+totalDistance+" miles");    
    //Above gives the total distance miles
  }
}