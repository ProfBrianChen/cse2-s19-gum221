//CSE2
//Gustav Masch
//HW04

import java.lang.Math;

public class PokerHandCheck{
  public static void main(String args[]){
    int randomCard;
    randomCard = (int) (Math.random()*(51+1));
    if (randomCard > 0 && randomCard < 14){
      //Conditional statement for assigning value range to diamond suit
      if(randomCard < 14 && randomCard > 10 || randomCard==1){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 13:
            //case of a King
            System.out.println("You picked the King of Diamonds");
            break;
          case 12:
            //case of a queen
            System.out.println("You picked the Queen of Diamonds");
            break;
          case 11:
            //case of a jack
            System.out.println("You picked the Jack of Diamonds");
            break;
          case 1:
            //case of an ace 
            System.out.println("You picked the Ace of Diamonds");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + randomCard + " of Diamonds");
      }
    }
    
    if (randomCard > 13 && randomCard < 27){
      //Conditional statement for assigning value range to clubs suit
      if(randomCard < 27 && randomCard > 23 || randomCard==14){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 26:
            //case of a King
            System.out.println("You picked the King of Clubs");
            break;
          case 25:
            //case of a queen
            System.out.println("You picked the Queen of Clubs");
            break;
          case 24:
            //case of a jack
            System.out.println("You picked the Jack of Clubs");
            break;
          case 14:
            //case of an ace
            System.out.println("You picked the Ace of Clubs");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard - 13) + " of Clubs");
      }
    }
    
    if (randomCard > 26 && randomCard < 40){
      //Conditional statement for assigning value range to spades suit
      if(randomCard < 40 && randomCard > 36 || randomCard==27){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 39:
            //case of a King
            System.out.println("You picked the King of Spades");
            break;
          case 38:
            //case of a queen
            System.out.println("You picked the Queen of Spades");
            break;
          case 37:
            //case of a jack
            System.out.println("You picked the Jack of Spades");
            break;
          case 27:
            //case of an ace
            System.out.println("You picked the Ace of Spades");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard - 26) + " of Spades");
      }
    }
    if (randomCard > 39){
      //Conditional statement for assigning value range to hearts suit
      if(randomCard < 53 && randomCard > 49 || randomCard==40 ){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 52:
            //case of a King
            System.out.println("You picked the King of Hearts");
            break;
          case 51:
            //case of a queen
            System.out.println("You picked the Queen of Hearts");
            break;
          case 50:
            //case of a jack
            System.out.println("You picked the Jack of Hearts");
            break;
          case 40:
            //case of an ace
            System.out.println("You picked the Ace of Hearts");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard - 39) + " of Hearts");
      }
    }
        int randomCard2;
    randomCard2 = (int) (Math.random()*(51+1));
    if (randomCard2 > 0 && randomCard2 < 14){
      //Conditional statement for assigning value range to diamond suit
      if(randomCard2 < 14 && randomCard2 > 10 || randomCard2==1){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard2){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 13:
            //case of a King
            System.out.println("You picked the King of Diamonds");
            break;
          case 12:
            //case of a queen
            System.out.println("You picked the Queen of Diamonds");
            break;
          case 11:
            //case of a jack
            System.out.println("You picked the Jack of Diamonds");
            break;
          case 1:
            //case of an ace 
            System.out.println("You picked the Ace of Diamonds");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + randomCard2 + " of Diamonds");
      }
    }
    
    if (randomCard2 > 13 && randomCard2 < 27){
      //Conditional statement for assigning value range to clubs suit
      if(randomCard2 < 27 && randomCard2 > 23 || randomCard2==14){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard2){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 26:
            //case of a King
            System.out.println("You picked the King of Clubs");
            break;
          case 25:
            //case of a queen
            System.out.println("You picked the Queen of Clubs");
            break;
          case 24:
            //case of a jack
            System.out.println("You picked the Jack of Clubs");
            break;
          case 14:
            //case of an ace
            System.out.println("You picked the Ace of Clubs");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard2 - 13) + " of Clubs");
      }
    }
    
    if (randomCard2 > 26 && randomCard2< 40){
      //Conditional statement for assigning value range to spades suit
      if(randomCard2 < 40 && randomCard2 > 36 || randomCard2==27){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard2){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 39:
            //case of a King
            System.out.println("You picked the King of Spades");
            break;
          case 38:
            //case of a queen
            System.out.println("You picked the Queen of Spades");
            break;
          case 37:
            //case of a jack
            System.out.println("You picked the Jack of Spades");
            break;
          case 27:
            //case of an ace
            System.out.println("You picked the Ace of Spades");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard2 - 26) + " of Spades");
      }
    }
    if (randomCard2 > 39){
      //Conditional statement for assigning value range to hearts suit
      if(randomCard2 < 53 && randomCard2 > 49 || randomCard2==40 ){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard2){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 52:
            //case of a King
            System.out.println("You picked the King of Hearts");
            break;
          case 51:
            //case of a queen
            System.out.println("You picked the Queen of Hearts");
            break;
          case 50:
            //case of a jack
            System.out.println("You picked the Jack of Hearts");
            break;
          case 40:
            //case of an ace
            System.out.println("You picked the Ace of Hearts");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard2 - 39) + " of Hearts");
      }
    }
    
        int randomCard3;
    randomCard3 = (int) (Math.random()*(51+1));
    if (randomCard3 > 0 && randomCard3 < 14){
      //Conditional statement for assigning value range to diamond suit
      if(randomCard3 < 14 && randomCard3 > 10 || randomCard3==1){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard3){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 13:
            //case of a King
            System.out.println("You picked the King of Diamonds");
            break;
          case 12:
            //case of a queen
            System.out.println("You picked the Queen of Diamonds");
            break;
          case 11:
            //case of a jack
            System.out.println("You picked the Jack of Diamonds");
            break;
          case 1:
            //case of an ace 
            System.out.println("You picked the Ace of Diamonds");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + randomCard3 + " of Diamonds");
      }
    }
    
    if (randomCard3 > 13 && randomCard3 < 27){
      //Conditional statement for assigning value range to clubs suit
      if(randomCard3 < 27 && randomCard3 > 23 || randomCard3==14){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard3){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 26:
            //case of a King
            System.out.println("You picked the King of Clubs");
            break;
          case 25:
            //case of a queen
            System.out.println("You picked the Queen of Clubs");
            break;
          case 24:
            //case of a jack
            System.out.println("You picked the Jack of Clubs");
            break;
          case 14:
            //case of an ace
            System.out.println("You picked the Ace of Clubs");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard3 - 13) + " of Clubs");
      }
    }
    
    if (randomCard3 > 26 && randomCard3 < 40){
      //Conditional statement for assigning value range to spades suit
      if(randomCard3 < 40 && randomCard3 > 36 || randomCard3==27){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard3){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 39:
            //case of a King
            System.out.println("You picked the King of Spades");
            break;
          case 38:
            //case of a queen
            System.out.println("You picked the Queen of Spades");
            break;
          case 37:
            //case of a jack
            System.out.println("You picked the Jack of Spades");
            break;
          case 27:
            //case of an ace
            System.out.println("You picked the Ace of Spades");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard3 - 26) + " of Spades");
      }
    }
    if (randomCard3 > 39){
      //Conditional statement for assigning value range to hearts suit
      if(randomCard3 < 53 && randomCard3 > 49 || randomCard3==40 ){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard3){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 52:
            //case of a King
            System.out.println("You picked the King of Hearts");
            break;
          case 51:
            //case of a queen
            System.out.println("You picked the Queen of Hearts");
            break;
          case 50:
            //case of a jack
            System.out.println("You picked the Jack of Hearts");
            break;
          case 40:
            //case of an ace
            System.out.println("You picked the Ace of Hearts");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard3 - 39) + " of Hearts");
      }
    }
    
        int randomCard4;
    randomCard4 = (int) (Math.random()*(51+1));
    if (randomCard4 > 0 && randomCard4 < 14){
      //Conditional statement for assigning value range to diamond suit
      if(randomCard4 < 14 && randomCard4 > 10 || randomCard4==1){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard4){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 13:
            //case of a King
            System.out.println("You picked the King of Diamonds");
            break;
          case 12:
            //case of a queen
            System.out.println("You picked the Queen of Diamonds");
            break;
          case 11:
            //case of a jack
            System.out.println("You picked the Jack of Diamonds");
            break;
          case 1:
            //case of an ace 
            System.out.println("You picked the Ace of Diamonds");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + randomCard4 + " of Diamonds");
      }
    }
    
    if (randomCard4 > 13 && randomCard4 < 27){
      //Conditional statement for assigning value range to clubs suit
      if(randomCard4 < 27 && randomCard4 > 23 || randomCard4==14){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard4){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 26:
            //case of a King
            System.out.println("You picked the King of Clubs");
            break;
          case 25:
            //case of a queen
            System.out.println("You picked the Queen of Clubs");
            break;
          case 24:
            //case of a jack
            System.out.println("You picked the Jack of Clubs");
            break;
          case 14:
            //case of an ace
            System.out.println("You picked the Ace of Clubs");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard4 - 13) + " of Clubs");
      }
    }
    
    if (randomCard4 > 26 && randomCard4 < 40){
      //Conditional statement for assigning value range to spades suit
      if(randomCard4 < 40 && randomCard4 > 36 || randomCard4==27){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard4){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 39:
            //case of a King
            System.out.println("You picked the King of Spades");
            break;
          case 38:
            //case of a queen
            System.out.println("You picked the Queen of Spades");
            break;
          case 37:
            //case of a jack
            System.out.println("You picked the Jack of Spades");
            break;
          case 27:
            //case of an ace
            System.out.println("You picked the Ace of Spades");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard4 - 26) + " of Spades");
      }
    }
    if (randomCard4 > 39){
      //Conditional statement for assigning value range to hearts suit
      if(randomCard4 < 53 && randomCard4 > 49 || randomCard4==40 ){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard4){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 52:
            //case of a King
            System.out.println("You picked the King of Hearts");
            break;
          case 51:
            //case of a queen
            System.out.println("You picked the Queen of Hearts");
            break;
          case 50:
            //case of a jack
            System.out.println("You picked the Jack of Hearts");
            break;
          case 40:
            //case of an ace
            System.out.println("You picked the Ace of Hearts");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard4 - 39) + " of Hearts");
      }
    }
        int randomCard5;
    randomCard5 = (int) (Math.random()*(51+1));
        if (randomCard5 > 0 && randomCard5 < 14){
      //Conditional statement for assigning value range to diamond suit
      if(randomCard5 < 14 && randomCard5 > 10 || randomCard5==1){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard5){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 13:
            //case of a King
            System.out.println("You picked the King of Diamonds");
            break;
          case 12:
            //case of a queen
            System.out.println("You picked the Queen of Diamonds");
            break;
          case 11:
            //case of a jack
            System.out.println("You picked the Jack of Diamonds");
            break;
          case 1:
            //case of an ace 
            System.out.println("You picked the Ace of Diamonds");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + randomCard5 + " of Diamonds");
      }
    }
    
    if (randomCard5 > 13 && randomCard5 < 27){
      //Conditional statement for assigning value range to clubs suit
      if(randomCard5 < 27 && randomCard5 > 23 || randomCard5==14){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard5){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 26:
            //case of a King
            System.out.println("You picked the King of Clubs");
            break;
          case 25:
            //case of a queen
            System.out.println("You picked the Queen of Clubs");
            break;
          case 24:
            //case of a jack
            System.out.println("You picked the Jack of Clubs");
            break;
          case 14:
            //case of an ace
            System.out.println("You picked the Ace of Clubs");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard5 - 13) + " of Clubs");
      }
    }
    
    if (randomCard5 > 26 && randomCard5 < 40){
      //Conditional statement for assigning value range to spades suit
      if(randomCard5 < 40 && randomCard5 > 36 || randomCard5==27){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard5){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 39:
            //case of a King
            System.out.println("You picked the King of Spades");
            break;
          case 38:
            //case of a queen
            System.out.println("You picked the Queen of Spades");
            break;
          case 37:
            //case of a jack
            System.out.println("You picked the Jack of Spades");
            break;
          case 27:
            //case of an ace
            System.out.println("You picked the Ace of Spades");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard5 - 26) + " of Spades");
      }
    }
    if (randomCard5 > 39){
      //Conditional statement for assigning value range to hearts suit
      if(randomCard5 < 53 && randomCard5 > 49 || randomCard5==40 ){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard5){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 52:
            //case of a King
            System.out.println("You picked the King of Hearts");
            break;
          case 51:
            //case of a queen
            System.out.println("You picked the Queen of Hearts");
            break;
          case 50:
            //case of a jack
            System.out.println("You picked the Jack of Hearts");
            break;
          case 40:
            //case of an ace
            System.out.println("You picked the Ace of Hearts");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard5 - 39) + " of Hearts");
      }
    }
    //Modulus all card values and then make the result an integer
    int cardDivision1=randomCard%13;
    int cardDivision2=randomCard2%13;
    int cardDivision3=randomCard3%13;
    int cardDivision4=randomCard4%13;
    int cardDivision5=randomCard5%13;
    
    //Create a count for the amount of pairs in the 5 cards
    int pairCount=0;
    //If statements for each random card to check if there are any pairs in the cards returned. Add a one to the pair counter if such is the case.
    if(cardDivision1==cardDivision2){
      pairCount += 1;
    }
    if(cardDivision1==cardDivision3){
      pairCount += 1;
    }
    if(cardDivision1==cardDivision4){
      pairCount += 1;
    }
    if(cardDivision1==cardDivision5){
      pairCount += 1;
    }
    if(cardDivision2==cardDivision5){
      pairCount += 1;
    }
    if(cardDivision2==cardDivision4){
      pairCount += 1;
    }
    if(cardDivision2==cardDivision3){
      pairCount += 1;
    }
    if(cardDivision3==cardDivision4){
      pairCount += 1;
    }
    if(cardDivision3==cardDivision5){
      pairCount += 1;
    }
    if(cardDivision4==cardDivision5){
      pairCount += 1;
    }
    //If the count variable is equal to one, then print that there is one pair in the cards
    if(pairCount==1){
      System.out.println("You have a Pair!");
    }
    //If the count variable is equal to two, then print that there are two pairs in the cards
    else if(pairCount==2){
      System.out.println("You have two Pairs!"); 
    }
    //Special else ifs to the case of a Four-of-a-kind
    else if(cardDivision1==cardDivision2&cardDivision2==cardDivision3&cardDivision3==cardDivision4){
      System.out.println("You have a four of a kind!"); 
    }
   else if(cardDivision2==cardDivision3&cardDivision3==cardDivision4&cardDivision4==cardDivision5){
      System.out.println("You have a four of a kind!"); 
    }
   else if(cardDivision1==cardDivision2&cardDivision2==cardDivision4&cardDivision4==cardDivision5){
      System.out.println("You have a four of a kind!"); 
    }
   else if(cardDivision1==cardDivision3&cardDivision3==cardDivision4&cardDivision4==cardDivision5){
      System.out.println("You have a four of a kind!"); 
    }
    //Create a counter for three same card values in the cards
    int ThreeCounter = 0;
    //If statements to encounter the possibility of a three of a kind in the cards
    if(cardDivision1==cardDivision2&cardDivision2==cardDivision3){
      System.out.println("You have a Three-of-a-Kind");
      ThreeCounter +=1;
    }
    if(cardDivision2==cardDivision3&cardDivision3==cardDivision4){
      System.out.println("You have a Three-of-a-Kind");
      ThreeCounter +=1;
    }
    if(cardDivision3==cardDivision4&cardDivision4==cardDivision5){
      System.out.println("You have a Three-of-a-Kind");
      ThreeCounter +=1;
    }
    if(cardDivision2==cardDivision3&cardDivision3==cardDivision5){
      System.out.println("You have a Three-of-a-Kind");
      ThreeCounter +=1;
    }
    
    //If statement to check if there is a full house by comparing the values of the three of a kind and pair counters
    if(ThreeCounter==1&&pairCount==1){
      System.out.println("You have a full-house!");
    }
    
    //final else if statement if no pairs, three of a kinds, four of a kinds or full houses are encountered
    else if(ThreeCounter==0&&pairCount==0){
      System.out.println("You have a high card!");
    }
  }
}
  