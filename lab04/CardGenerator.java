//CSE002
//Gustav Masch
//Lab 04
//In this lab we will create a random card number generator that employs both if and switch conditional statements

import java.lang.Math;

public class CardGenerator{
  //main method for every java program
  public static void main(String args[]){
    int randomCard;
    //define the random card variable as an integer
    randomCard = (int) (Math.random()*(51+1));
    //using the random number generator, we find the value of the random card. We set the range from 1 to 52
    if (randomCard > 0 && randomCard < 14){
      //Conditional statement for assigning value range to diamond suit
      if(randomCard < 14 && randomCard > 10 || randomCard==1){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 13:
            //case of a King
            System.out.println("You picked the King of Diamonds");
            break;
          case 12:
            //case of a queen
            System.out.println("You picked the Queen of Diamonds");
            break;
          case 11:
            //case of a jack
            System.out.println("You picked the Jack of Diamonds");
            break;
          case 1:
            //case of an ace 
            System.out.println("You picked the Ace of Diamonds");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + randomCard + " of Diamonds");
      }
    }
    
    if (randomCard > 13 && randomCard < 27){
      //Conditional statement for assigning value range to clubs suit
      if(randomCard < 27 && randomCard > 23 || randomCard==14){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 26:
            //case of a King
            System.out.println("You picked the King of Clubs");
            break;
          case 25:
            //case of a queen
            System.out.println("You picked the Queen of Clubs");
            break;
          case 24:
            //case of a jack
            System.out.println("You picked the Jack of Clubs");
            break;
          case 14:
            //case of an ace
            System.out.println("You picked the Ace of Clubs");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard - 13) + " of Clubs");
      }
    }
    
    if (randomCard > 26 && randomCard < 40){
      //Conditional statement for assigning value range to spades suit
      if(randomCard < 40 && randomCard > 36 || randomCard==27){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 39:
            //case of a King
            System.out.println("You picked the King of Spades");
            break;
          case 38:
            //case of a queen
            System.out.println("You picked the Queen of Spades");
            break;
          case 37:
            //case of a jack
            System.out.println("You picked the Jack of Spades");
            break;
          case 27:
            //case of an ace
            System.out.println("You picked the Ace of Spades");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard - 26) + " of Spades");
      }
    }
    if (randomCard > 39){
      //Conditional statement for assigning value range to hearts suit
      if(randomCard < 53 && randomCard > 49 || randomCard==40 ){
        //conditional statement to exclude king, queen, jack and ace from card range
        switch(randomCard){
            //switch statement to classify if randomCard is a jack queen king or ace based on the above defined range 
          case 52:
            //case of a King
            System.out.println("You picked the King of Hearts");
            break;
          case 51:
            //case of a queen
            System.out.println("You picked the Queen of Hearts");
            break;
          case 50:
            //case of a jack
            System.out.println("You picked the Jack of Hearts");
            break;
          case 40:
            //case of an ace
            System.out.println("You picked the Ace of Hearts");
            break;             
        }
      }
      else{
        //else statement to print the value of the card if the randomCardard value does not fulfill the if statement range
        System.out.println("You picked the " + (randomCard - 39) + " of Hearts");
      }
    }    
  }
}