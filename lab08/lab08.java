//CSE002
//Gustav Masch
//In this program we will generate an array of random size from 50 to 100 entries
//and populate it with numbers between 0-99 inclusive. We will calculate the range,
//mean and standard deviation with the use of methods for each individual calculation
//we will also re-sort the numbers after the calculations are completed and printed

import java.util.*;
public class lab08 {

    public static void main(String[] args) {

        int randa = (int)(Math.random()*51+50); //generate a random int between 50-100 for the size of the array

        int[] asize;
        asize = new int [randa];//assign generated random value to the size of the array
        System.out.println("The size of the array is "+randa);


        for(int i=0; i<asize.length; i++){//loop to generate each random value for each index of the array
            int randv = (int)(Math.random()*100);
            asize[i]=randv;
            System.out.println(asize[i]);
        }

        int range = getRange(asize);//method to estimate the range
        System.out.println("The range of the array is: " + range);

        double mean = getMean(asize);//method to estimate the mean
        System.out.println("The mean of the array is: " + mean);

        double stddev = getStdDev(asize,mean);//method to estimate the standard deviation
        System.out.println("The standard deviation is: "+stddev);

        shuffle(asize);//method to reshuffle the array

    }

    public static int getRange (int[]size){//method to estimate the range of the values
        Arrays.sort(size);//sort array from small to large
        int minValue = size[0];//find min and max values
        int maxValue = size[(size.length-1)];
        System.out.println("Max number is: "+maxValue);
        System.out.println("Min number is: "+minValue);
        int range = maxValue-minValue;//calculate range
        return range;
    }

    public static double getMean (int[]size){//method to calculate the mean of the array
        int sum = 0;
        for(int i = 0; i<size.length;i++){//for loop to sum each index of the array
         sum = sum + size[i];
        }
        double mean = sum/size.length;//divide sum of each index by the length
        return mean;
    }

    public static double getStdDev(int[]size, double mean){//method to calculate the standard deviation
        double sum = 0;
        for(int i = 0; i<size.length; i++){//get the sum of each index minus the mean squared
            sum = sum + Math.pow((size[i]-mean),2);
        }
        double stddev = Math.sqrt(sum/(size.length-1));// calculate standard deviation
        System.out.println("The length of array is: "+size.length);
        return stddev;
    }

    public static void shuffle(int[]size){//static void method to print the reshuffled array
        for(int i=0; i<size.length;i++){//loop to re shuffle the indexes of the array
            int target = (int) (size.length * Math.random());
            int temp = size[target];
            size[target] = size[i];
            size[i] = temp;
            System.out.println(size[i]);//re print array
        }

    }
}
