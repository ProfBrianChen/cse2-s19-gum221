//CSE02
//Gustav Masch
//lab09
//In this lab we will have two method. The first one that creates an array and fills it with random numbers within the range
// of the array lenght or that creates an array with random ascending numbers. The second method will use either binary search,
// for the first type array or binary search for the second type of generated array.

import java.util.*;
public class lab09 {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Do you want to perform linear or binary search? : ");
        //ask the user to provide what search method they wanna use
        String linear = "linear";
        String binary = "binary";
        String input="";
        input = myScanner.next();
        //if the user enters equal the following if statement runs
        if(input.equals(linear)){
            System.out.println("Enter the desired array size: ");
            //prompt user to enter the wanted array size
            int arrsize = myScanner.nextInt();
            System.out.println("Enter the integer search term: ");
            //prompt user to enter the number to be searched
            int search = myScanner.nextInt();
            //run array generator method
            int [] arrayA = getArrayA(arrsize);
            //run linear search method
            int loc = linsearch(arrayA,search);
            print(arrayA);
            //print array and index location of the searched number if there exist one
            System.out.println("The location is: "+loc);
        }else if(input.equals(binary)){
            System.out.println("Enter the desired array size: ");
            //prompt user to enter the wanted array size
            int arrsize2 = myScanner.nextInt();
            System.out.println("Enter the integer search term: ");
            //prompt user to enter the number to be searched
            int search2 = myScanner.nextInt();
            //run array generator method
            int [] arrayB = getArrayB(arrsize2);
            //run binary search method
            int loc2 = binsearch(arrayB,search2);
            print(arrayB);
            //print array and index location of the searched number if there exist one
            System.out.println("The location is: "+loc2);
        }

    }

    public static int[] getArrayA(int input){//method for generating arrays to be used by linear search method
        int size  = input;
        int [] array1 = new int [size];
        for(int i = 0; i<array1.length; i++){
            int randv = (int)(Math.random()*(array1.length));
            array1[i] = randv;
        }
        return array1;
    }

    public static int[] getArrayB(int input){//method for generating arrays to be used by binary search method
        int size  = input;
        int [] array1 = new int [size];
        Random myrandom = new Random();
        int rand = myrandom.nextInt();
        if (rand<0){
            rand = rand*(-1);
        }

        for(int i = 0; i<array1.length; i++){
            array1[i] = rand++;
        }
        return array1;
        /*int size  = input;
        int [] array1 = new int [size];
        for(int i = 0; i<array1.length; i++){
            int randv = (int)(Math.random()*(array1.length));
            array1[i] = randv;
        }
        return array1;*/
    }//The instructions were difficult to understand as they didnt specify the range of random numbers for array B,
     //just that they had to be in ascending order, so that is basically what I tried to do here, a method that would
     //generate ANY positive random number, as that is what the instructions stated

    public static int linsearch(int input[],int n){
      //basic method to conduct the linear search
        int [] arr = input;
        int x = n;
    for(int i = 0; i < arr.length; i++) {
        if(arr[i] == x)
            return i;
    }
    return -1;
    }

    public static int binsearch(int input[], int n){
      //method for binary search
        int [] arr = input;
        int l = 0;
        int x = n;
        int r = arr.length;

        while (l < r) {
            int m = l + (r - l) / 2;
            if (arr[m] == x)
                return m;

            if (arr[m] < x)
                l = m + 1;

            else
                r = m - 1;
        }
        return -1;
    }

    public static void print(int input[]){
      //basic method for printing arrays
        int [] input1 = input;
            System.out.print("The random arrays is: {");
            for(int i=0; i<input1.length; i++){
                if(i<(input1.length-1)){
                    System.out.print(input1[i]+",");
                }else{
                    System.out.print(input1[i]);
                }
            }
            System.out.print("}");
            System.out.println();
    }
}
