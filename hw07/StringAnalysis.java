//CSE002
//Gustav Masch
//HW07
//In this programm we will be processing a string written by the user and determining if the containing characters
//are letters or not letters


import java.util.*;

public class StringAnalysis{
  public static void main(String[] args){ //main method in every java programm

    Scanner myScanner = new Scanner(System.in); //initialize scanner method
    System.out.println("Enter a string"); //prompt user to enter a string
    String s = myScanner.next(); //save the string entered by the user

    System.out.println("Do you want to examine all letters? (yes/no, case sensitive!)"); //ask user if all the characters should be analyzed or just a number
    String yes = "yes"; //establish strings for the users answer
    String no = "no";

    boolean valid = false; //while loop to check if the user entered a valid "yes" or "no", if not, prompt the user to enter yes or no again
        String input = "";
        while(!valid)
        {
            input = myScanner.next();
            if(input.equals(yes) || input.equals(no)){
                valid = true;
            } else {
                System.out.println("Please enter yes or no");
            }
        }

    if(input.equals(yes)){ //if statement to check all the characters in the case the user enters yes
      for( int i = 0; i<s.length(); i++){ //for loop to check all the characters in the string
        char charAt = s.charAt(i); //create char variable for each character in the string
        boolean letter = isletter(charAt); //boolean for the method checking if the character is a letter or not
        if(letter==true){ //if statement to print if the character is a letter or not
          System.out.println("Character " + charAt + " is a letter");
        }
        else{
          System.out.println("Character " + charAt + " is not a letter");
        }
      }
    }

    else if(input.equals(no)){ //if statement to check all the characters in the case the user enters no
      System.out.println("How many characters do you want to check?"); //prompt user to state how many characters should be checked
      int charNumber = getNumber(); //check if the integer entered by the user is valid with a method
      for(int i=0; i<charNumber; i++){ //for loop to check individual character, same as the one above
        char charAt = s.charAt(i);
        boolean letter = isletter(charAt);
        if(i==s.length()){ //if i is already at the length of string break for loop
          break;
        }
        if(letter==true){
          System.out.println("Character " + charAt + " is a letter");
        }
        else{
          System.out.println("Character " + charAt + " is not a letter");
        }
      }
    }

  }


  public static int getNumber () { //method to check if the user entered an int
        Scanner myScanner = new Scanner(System.in);
        while(!myScanner.hasNextInt()||myScanner.nextInt()<1) {
            System.out.println("Please enter a double");
            myScanner.next();
        }
        return myScanner.nextInt();
    }

  public static boolean isletter (char letter){//method to check if the character of the string entered by the user is a letter or not
    boolean value =false;
    if(Character.isLetter(letter)==true){
      value =true;
    }
    else{
      value = false;
    }
      return value;
    }

  }
