//CSE002
//Gustav Masch
//HW07
//In this programm we will calculate the are of three shapes based on what the user enters

import java.util.*;

public class Area {

    public static void main(String[] args) {// main method in every java programm
        Scanner myScanner = new Scanner(System.in); //initiate scanner method
        System.out.println("Enter the name of the desired shape (triangle,rectangle or circle)"); //prompt user to enter desired shape


        String triangle = "triangle"; //create strings for each shape the user decides to pick
        String rectangle = "rectangle";
        String circle = "circle";

        boolean valid = false; //while loop to check if the user entered a valid shape
        String input = "";
        while(!valid)
        {
            input = myScanner.next();
            if(input.equals(triangle) || input.equals(rectangle) || input.equals(circle)){
                valid = true;
            } else {
                System.out.println("Please enter traingle, rectangle or circle");
            }
        }

        if(input.equals(triangle)){//if statement to calculate triangle area if that is what the user entered
            System.out.println("Enter base size");//prompt user to enter base size
            double base = getNumber();//use the getNumber method to check if the integer entered by the user is valid
            System.out.println("Enter height");//prompt user to enter height
            double height = getNumber();//use the getNumber method to check if the integer entered by the user is valid
            double area = triArea(base,height);//Use the area specific method to calculate the area of the shape
            System.out.println("The area of your triangle is " +area);//print result
        }
        if(input.equals(rectangle)){
            System.out.println("Enter base size");
            double base = getNumber();//use the getNumber method to check if the integer entered by the user is valid
            System.out.println("Enter height");
            double height = getNumber();//use the getNumber method to check if the integer entered by the user is valid
            double area = rectArea(base,height);//Use the area specific method to calculate the area of the shape
            System.out.println("The area of your rectangle is " +area);//print result
        }
        if(input.equals(circle)){
            System.out.println("Enter the radius of the circle");
            double radius = getNumber();//use the getNumber method to check if the integer entered by the user is valid
            double area = cirArea(radius);//Use the area specific method to calculate the area of the shape
            System.out.println("The area of your circle is " +area);//print result
        }

    }

    public static double triArea (double base, double height){//method to calculate the are of a triangle
       return base*height*0.5;
    }

    public static double rectArea (double base, double height){//method to calculate the area of a rectangle
        return base*height;
    }

    public static double cirArea (double radius){//method to calculate the area of a circle
        int i = 2;
        return 3.141512*(Math.pow(radius,2));
    }

    public static double getNumber () {//method to check if the double entered by the user is a valid double
        Scanner myScanner = new Scanner(System.in);
        while(!myScanner.hasNextDouble()||myScanner.nextDouble()<1) {
            System.out.println("Please enter a positive double");
            myScanner.next();
        }
        return myScanner.nextDouble();
    }

}
