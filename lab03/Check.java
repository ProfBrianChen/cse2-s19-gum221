//Gustav Masch
//CSE002 
//LAB03: Check
//The purpose of the lab is to create a program that recieves an input, 
//in this case the bill of a restaurant, and creates an output, 
//in this case the cheks delivered to each costumer
import java.util.Scanner;
//
//
//
public class Check{
  //main method required for every Java program
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    //Scanner alllows user to input values into the program
    System.out.print("Enter the original cost of the check in the form xx.xx: $");
    //Message to the user prompting him to enter the amount of the check
    double checkCost = myScanner.nextDouble();
    //Define the value of the cost of the chek by using myScanner to take the number inserted by the user
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    double totalCost;
    double costPerPerson;
    int dollars, 
    //whole dollar amount of cost
    dimes, pennies;
    //for storing digits to the right of the decimal point for the cost$
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost/numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g.
    //where the % (mod) operator returns the remainder
    //after the division:   583%100 -> 83, 27%5 -> 2
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    

  }// end of main method
}// end of class