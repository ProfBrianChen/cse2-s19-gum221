//CSE002
//Gustav Masch
//In this program we will generate a string of random size between 20-10 and ask the user if
//the generated matrix should be shortened or merged with another random matrix of the same
//size range

import java.util.*;
public class hw09 {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Do you want to run insert or shorten?: ");

        String insert = "insert";// create multiple strings to recognize user input
        String shorten = "shorten";
        String input="";
        input = myScanner.next();

        if(input.equals(insert)){//if statement to check if user entered insert
            int []input1 = generate();//generate input1 with the generate method
            System.out.print("Input1: ");
            print(input1);
            int []input2 = generate();//generate input2 with the generate method
            System.out.print("Input2: ");
            print(input2);
            int []output = insert(input1, input2);//run insert method to generate output array
            System.out.print("Output: ");
            print(output);//use print method to print output matrix
        }else{//else statement to run shorten
            int []input1 = generate();//generate input1
            System.out.print("Input1: ");
            print(input1);//print method to print input1
            int []output=shorten(input1);//run shorten method
            if(output[(output.length-1)]!=0){
              //I found no other way to check if the input2 in the method of shortened was longer
              //than the length so I created an if statement to check if the returned matrix contained only zeros
                System.out.print("Output: ");
                print(output);
            }else{
                System.out.print("Output: ");
                print(input1);
            }
        }
    }

    public static int[] generate(){//generate method to create random arrays and numbers
        int rand = (int)(Math.random()*11+10);
        int[] input1;
        input1 = new int [rand];
        System.out.println("The size of the array is "+rand);
        int numbers = 1;

        for(int i=0; i<input1.length; i++){
            int randv = (int)(Math.random()*(input1.length));
            input1[i]=randv;
        }

        return input1;
    }

    public static void print(int input[]){//void method to print arrays
        int [] input1 = input;
            System.out.print("{");
            for(int i=0; i<input1.length; i++){
                if(i<(input1.length-1)){
                    System.out.print(input1[i]+",");
                }else{
                    System.out.print(input1[i]);
                }
            }
            System.out.print("}");
            System.out.println();
    }

    public static int[] insert(int input1[], int input2[]){//insert method to generate output array with two inputs
        int[]a = input1;
        int[]b = input2;
        int loc = (int)(Math.random()*input1.length);
        System.out.println("loc is: "+loc);
        int[]d = new int[a.length+b.length];
        int count = 0;
        int counta = 0;

        for(int i = 0; i < loc ; i++) {
            d[i] = a[i];
            count++;
            counta++;
        }
        for(int j = 0; j < b.length;j++) {
            d[count++] = b[j];
        }
        int temp = count;
        System.out.println("Count is:" + count);System.out.println("Counta is:" + counta);
        for(int k = temp; k < d.length;k++) {
            d[count++] = a[counta++];
        }
        return d;
    }

    public static int[] shorten(int input1[]){//shorten method to shorten input array
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Input2: ");
        int input2 = myScanner.nextInt();
        int [] input = input1;
        int [] newarray = new int[input.length - 1];

        if(input2<=input.length){
           int j = 0;
           for(int i = 0; i < input.length; i++) {
                if(i == input2) {
                i++;
            }
            newarray[j++] = input[i];
            }
        }
        return newarray;
    }


}
