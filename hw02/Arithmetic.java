//Gustav Masch
//CSE2
//HW02
//The purpose of this homework is to calculate the bill of the purchase 
//of various garments. Also, we will be learning how to cast numbers 
//in order to, in this case, have only two decimal points.

public class Arithmetic{
  
  public static void main(String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    //total cost of pants
    
    double CostOfPants=(numPants*pantsPrice)*100;
    int intCoP=(int)CostOfPants;
    double CoPants=intCoP/100;
    //Calculating the total Cost of the pants and converting the result to a number with only two decimal points.
    double CostOfShirts=(numShirts*shirtPrice)*100;
    int intCoS=(int) CostOfShirts;
    double CoShirts=intCoS/100;
    //Calculating the total Cost of the shirts and converting the result to a number with only two decimal points.
    double CostOfBelts=(numBelts*beltPrice)*100;
    int intCoB=(int) CostOfBelts;
    double CoBelts=intCoB/100;
    //Calculating the total Cost of the belt and converting the result to a number with only two decimal points.
    double PantsTax=CoPants*paSalesTax;
    double ShirtsTax=CoShirts*paSalesTax;
    double BeltsTax=CoBelts*paSalesTax;
    //Calculating the tax for each garment according to pennsylvania's 6% VAT.
    double TotalCost=CoBelts+CoShirts+CoPants;
    double TotalTax=PantsTax+BeltsTax+ShirtsTax;
    double GrandTotal=TotalTax+TotalCost;
    //adding each component of the costs, taxes in order to get the total cost, total tax, and the grand total of cost plus tax
    System.out.println("The cost for the Pants is $"+CoPants);
    System.out.println("The Tax for the Pants is $"+PantsTax);
    System.out.println("The cost for the Shirts is $"+CoShirts);
    System.out.println("The Tax for the Shirts is $"+ShirtsTax);
    System.out.println("The cost for the Belt is $"+CoBelts);
    System.out.println("The Tax for the Belt is $"+BeltsTax);
    System.out.println("The total cost is $"+TotalCost);
    System.out.println("The total tax is $"+TotalTax);
    System.out.println("The Grand total is $"+GrandTotal);
    //printing the above calculated results to the user
  }
}