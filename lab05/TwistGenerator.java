//Lab05
//CSE002
//Gustav Masch
//In this lab we will produce a "twist", which is a pattern made by slashes and x's. The user will determine the length of said "twist"
//by choosing its length. We will achieve the pattern by using a series of while loops combined with if and else if statements

import java.util.*;

public class TwistGenerator {
    public static void main(String[] args) {
        //initiate scanner for user to enter number
        Scanner myScanner = new Scanner(System.in);
        //prompt user to enter number
        System.out.println("Enter non-negative length integer: ");
        //create boolean to verify if user enterd an integer
        boolean trueint = myScanner.hasNextInt();
        //ccheck if integer is positive
        while(trueint != true){
          String junkWord = myScanner.next();
          System.out.println("Enter a valid integer");
          trueint = myScanner.hasNextInt();
        }//create a variable to store users number
          int intval = myScanner.nextInt();     
      //initiate count for top line
                int counttop=0;
                //initiate count for bottom line
                int countbottom=0;
                //initiate count for middle line
                int countx=0;
                //while loop for slashes in top line
                while(intval>counttop){
                    if(counttop%3==0){
                        System.out.print("\\");
                }
                    else if(counttop%3==1){
                        System.out.print(" ");
                    }
                    else if(counttop%3==2){
                        System.out.print("/");
                    }
                    
                    counttop +=1;
                }
                System.out.print("\n");
                //while loop for determining the amount of middle x's
                while(intval>countx){
                    if(countx%3==0){
                        System.out.print(" ");
                }
                    else if(countx%3==1){
                        System.out.print("X");
                    }
                    else if(countx%3==2){
                        System.out.print(" ");
                    }
                    
                    countx +=1;
                }
                System.out.print("\n");
                //while loop for determining the amount of slashes in bottom line
                while(intval>countbottom){
                    if(countbottom%3==0){
                        System.out.print("/");
                }
                    else if(countbottom%3==1){
                        System.out.print(" ");
                    }
                    else if(countbottom%3==2){
                        System.out.print("\\");
                    }
                    
                    countbottom +=1;
                }
        //string to eliminate value in the scanner    
        String junkWord = myScanner.next();
        }
        // TODO code application logic here
   
}