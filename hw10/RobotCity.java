//CSE002
//Gustav Masch
//Hw10
//In this program we will generate a rectangular matrix of a random dimension between 10-15. Then, we will proceed
//to fill it with random integers between 0-999. Subsequently, robots will randomly appear in the matrix
//(denoted by a negative sign in the assigned index) and make them move 5 times to the right (West) of the city

public class RobotCity {

    public static void main(String[] args) {
        // TODO code application logic here
        int[][] city = buildCity();
        display(city);
    }


    public static int[][] buildCity(){

        int[][]city = new int [(int)(Math.random()*6)+10][(int)(Math.random()*6)+10];

        for (int row = 0; row < city.length; row++) {
            for (int col = 0; col < city[row].length; col++) {
                city[row][col] = (int)(Math.random()*900)+100;
            }
        }

        return city;
    }

    public static void display(int array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[0].length; j++){
                System.out.printf("%s ", array[i][j]);
            }
            System.out.println();
        }
    }
}
