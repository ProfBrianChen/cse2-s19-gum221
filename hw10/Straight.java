//CSE002
//Gustav Masch
//Hw10
//In this program we will generate a Card deck, randomly select 5 cards out of the array and then check if they contain
// a straight. We will also run the program a million times and check for the probability of getting a straight

import java.util.*;

public class Straight {

    public static void main(String[] args) {
        // TODO code application logic here
        int scount=0;
        int k = 1;
        for(int i = 0; i<1000000;i++){
            int [] deck = generateDeck();
            int [] hand = drawCards(deck);
            boolean straight = containStraight(hand);
            if(straight==true){
                scount++;
            }
        }


        double probability = (scount/1000000);
        System.out.println("There were "+scount+" straights");
        System.out.println("The probability of getting a straight is: "+probability+"%");

    }

    public static int[] generateDeck(){
        int[] shuffleDeck = new int [52];
        Random rgen = new Random();
        int count = 0;

        for(int i = 0; i<shuffleDeck.length;i++){
            shuffleDeck[i] = count++;
        }
        for(int j = 0; j<shuffleDeck.length;j++){
            int target = rgen.nextInt(shuffleDeck.length);
            int temp = shuffleDeck[target];
            shuffleDeck[target] = shuffleDeck[j];
            shuffleDeck[j] = temp;
        }

        return shuffleDeck;
    }

    public static int[] drawCards(int[] array){
        int []hand = new int[5];

        for(int i = 0; i<hand.length; i++){
            hand[i]=array[i];
        }
        return hand;
    }

    public static void cardSearch(int[] array, int k){
        Arrays.sort(array);
        if(k>5||k<0){
            System.out.println(" error ");
        }else{
            System.out.println("The "+k+" lowest card is: "+array[k]);
        }
    }

    public static boolean containStraight(int[]hand){
        if(hand == null || hand.length != 5) {
        return false;
    }
    else {
        Arrays.sort(hand);
        int prev = -1;

        for(int i = 0; i < hand.length; i++) {

            if(prev == -1 || (prev + 1) == hand[i]) {
                prev = hand[i];
            }
            else {
                return false;
            }
        }

        return true;
        }
    }

}
