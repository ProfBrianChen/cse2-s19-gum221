//CSE02
//Gustav Masch
//In this lab  we will be generating various patterns of numbers using nested loops

import java.util.*;
public class PatternA {

    public static void main(String[] args) {
      //main method in every java program
        Scanner myScanner = new Scanner(System.in);
        //initiate scanner command
        int input = -1;
        //initiate input variable for the user
        boolean valid = false;
        //initiate boolean variable for the input test

        while(valid == false) {
          // while loop to check the user entered value
            System.out.println("Enter an integer between 1-10");
            //prompt user to enter an integer between 1-10
            if(myScanner.hasNextInt()){
              //check if the value entered is an integer
                input = myScanner.nextInt();
            } else {
                myScanner.next();
            }
            if(input == (int)input && input < 10 && input > 0) {
              //check if the entered value is an integer and is between 1 and 10
                valid = true;
                //nested loop to create a descending pattern of numbers using the integer entered by the user as a reference
                for(int numRows = 1; numRows <= input; numRows++){
                    for(int i = 1; i<=numRows ; i++){
                        System.out.print(i+" ");
                    }
                System.out.println("");
                }
            } else {
              //prompt user to enter a new number
                System.out.println("Invalid Try Again");
            }

        }

    }

}