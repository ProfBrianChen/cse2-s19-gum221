//CSE02
//Gustav Masch
//In this lab  we will be generating various patterns of numbers using nested loops

import java.util.Scanner;
public class PatternC {

    public static void main(String[] args) {
        //main method in every java program
        Scanner myScanner = new Scanner(System.in);
        //initiate scanner command
        int input = -1;
        //iniate input value for user
        boolean valid = false;
        //iniate boolean variable to test user entry

        while(valid == false) {
          //prompt user to enter a number
            System.out.println("Enter an integer between 1-10");
            if(myScanner.hasNextInt()){
              //Check if user entered an integer
                input = myScanner.nextInt();
            } else {
                myScanner.next();
            }
            if(input == (int)input && input < 10 && input > 0) {
              //if the user entered an integer and it is between 1 and 1o proceed with pattern
                valid = true;
                for(int numRows = 1; numRows <= input; numRows++){
                  //generate pattern
                    for(int space = (input-1); space>=numRows; space--){
                        System.out.print(" ");
                    }
                    for(int i = 1; i<=numRows ; i++){
                        System.out.print(i);
                    }
                    System.out.println("");
                }
            } else {
                System.out.println("Invalid Try Again");
            }

        }

    }

}
