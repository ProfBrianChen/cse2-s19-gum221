//CSE02
//Gustav Masch
//In this lab  we will be generating various patterns of numbers using nested loops

import java.util.*;
public class PatternD {

    public static void main(String[] args) {
        //main method in every java program
        Scanner myScanner = new Scanner(System.in);
        //initiate scanner command
        int input = -1;
        //initiate input variable for the user entered value
        boolean valid = false;
        //initiate boolean variable for user entered value test
        while(valid == false) {
          //while loop to check the value entered by the user
            System.out.println("Enter an integer between 1-10");
            //prompt user to enter an integer
            if(myScanner.hasNextInt()){
              //check if the entered value is an integer
                input = myScanner.nextInt();
            } else {
                myScanner.next();
            }
            if(input == (int)input && input < 10 && input > 0) {
              //check if the entered value is an integer and is between 1 and 10
                valid = true;
                //nested loop to create pattern of numbers
                for(int numRows = input; numRows >= 1; numRows--){
                    for(int i=numRows; i>=1; i--){
                        System.out.print(i+" ");
                    }
                    System.out.println();
                }
            } else {
              //prompt the user to enter a valid integer
                System.out.println("Invalid Try Again");
            }

        }


    }
}
