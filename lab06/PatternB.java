//CSE02
//Gustav Masch
//In this lab  we will be generating various patterns of numbers using nested loops

import java.util.*;
public class PatternB {


    public static void main(String[] args) {
        //main method for every java program
        Scanner myScanner = new Scanner(System.in);
        //initiate scanner command
        int input = -1;
        //initiate input variable for the user entered character
        boolean valid = false;
        //initiate boolean variable for the test of the user entered value

        while(valid == false) {
          //while loop to check the entered number
            System.out.println("Enter an integer between 1-10");
            //prompt user to enter a number
            if(myScanner.hasNextInt()){
              //check if the entered value is an int
                input = myScanner.nextInt();
            } else {
                myScanner.next();
            }
            if(input == (int)input && input < 10 && input > 0) {
              //check if the entered number is an int and is between 1 and 10
                valid = true;
                //nested loop to create pattern
                int inputn = input;
                for(int numRows = input; numRows >=1; numRows--){
                    for(int i=1; i<=numRows; i++){
                        System.out.print(i+" ");
                    }

                    System.out.println("");
                }

            } else {
              //prompt user to enter a valid number
                System.out.println("Invalid Try Again");
            }

        }


    }

}
