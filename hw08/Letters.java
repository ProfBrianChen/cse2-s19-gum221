//CSE002
//Gustav Masch
//In this program we will generate an arbitrary amount of chars and create an array with size of
//the arbitrary amount of Characters

import java.util.*;
public class Letters {

    public static void main(String[] args) {

        int[] size;
        size = new int [10]; //Define and initiate an array of an arbitrary size (I used 10 but im not sure of this had to be a random number?)

        System.out.print("Random character array: ");

        for(int i=0; i<size.length; i++){//Generate random Characters of random cases
            double randcase = Math.random();
            if(randcase >= 0.5){//Using Math.random(), we can randomize if the character is upper or lower case
                int randupcase = (int)(Math.random()*26+65);
                //cast Math.random into an int and generate an int between 65-90, which correspond to the ascii values of the upper-case abecedary
                int upC = randupcase;//initiate each int corresponding to each index of the array
                size[i]=upC;//assign each int to the corresponding index
            }else{
                int randlowcase = (int)(Math.random()*26+97);
                //cast Math.random into an int and generate an int between 97-122, which correspond to the ascii values of the upper-case abecedary
                int lowC = randlowcase;//define and initialize an int corresponding to the above generated random value
                size[i]=lowC;//assign each int to the corresponding array
            }
        }
        for(int i = 0; i<size.length; i++){//print the random characters generated
            char print= (char)size[i];//cast the ints into chars, as they correspond to a char either upper or lower case
            System.out.print(print);
        }

        System.out.println();

        char[] atom = getAtoM(size);// use AtoM method to check which characters in the array are between a and m
        System.out.print("AtoM Characters: ");
        for(int i = 0; i<atom.length;i++){//print the array
            char print = atom[i];
            System.out.print(print);
        }
        System.out.println();
        System.out.print("NtoZ Characters: ");

        char[] ntoz = getNtoZ(size);// use NtoZ method to check which characters in the array are between a and m
        for(int i = 0; i<ntoz.length;i++){
            char print= (char)ntoz[i];
            System.out.print(ntoz[i]);
        }
        System.out.println();

    }

    public static char[] getAtoM(int size[]){//check if characters are between a to m

        int ittr = 0;
        char[] atom= new char[size.length];
        for(int i=0; i<size.length;i++){
            if (size[i]<78 || ( size[i]>95 && size[i]<109 ) ){
                atom[ittr]=(char)size[i];//assign which characters which are in the array are in the new array of only a to m Characters
                ittr++;
            }
        }
        char[] rtn = new char[ittr];

        for(int i = 0; i < ittr; i++){
            rtn[i] = (char)atom[i];//reassign values from the array to a new array which only contains the characters between a and m
        }
        return rtn;
    }

    public static char[] getNtoZ(int size[]){

        int ittr = 0;
        char[] ntoz= new char[size.length];
        for(int i=0; i<size.length;i++){
            if (size[i]>77&&size[i]<91||size[i]>108 && size[i]<123  ){
                ntoz[ittr]=(char)size[i];//assign which characters which are in the array are in the new array of only n to z values
                ittr++;
            }
        }
        char[] rtn = new char[ittr];

        for(int i = 0; i < ittr; i++){
            rtn[i] = (char)ntoz[i];//reassign values from the array to a new array which only contains the characters between n and z
        }
        return rtn;

    }

}
