//CSE002
//Gustav Masch
//In this program we will ask the user to enter five numbers and generate other five numbers
//between 0-59 with no substitution
import java.util.*;


public class Playlottery {

    public static void main(String[] args) {//main method in every java program

        Scanner myScanner = new Scanner(System.in);//initiate scanner
        System.out.println("Enter 5 numbers between 0-59");//prompt user to enter numbers between 0-59

        int[] userpick = new int[5];//assign each variable from the user to each index of the array
        for(int i = 0; i<userpick.length;i++){
            System.out.print("Enter number "+(i+1)+": ");
            userpick[i] = myScanner.nextInt();
        }

        System.out.println();

        System.out.print("Your numbers are: [");//print the user-entered numbers
        for(int i = 0; i<userpick.length;i++){
            System.out.print(" "+userpick[i]);
        }
        System.out.print(" ]");
        System.out.println();


        int[] numpick;
        numpick = numbersPicked();//generate an array with random numbers
        System.out.print("Lottery numbers are: [");
        for(int i = 0; i<numpick.length;i++){
            System.out.print(" "+numpick[i]);
        }
        System.out.print(" ]");
        System.out.println();

        boolean winorlose = lotterywin(userpick,numpick);//method to check if all of the numbers match
        if (winorlose){//tell the user if the chosen numbers are equal to the generated lottery numbers
            System.out.println("You won!!!");
        }else{
            System.out.println("You lost! better luck next time.");
        }
    }

    public static int[] numbersPicked(){//method to generate 5 random numbers
        int[] numbers = new int[5]; //declare new array with five indexes
        Random r = new Random();

        for(int i=0; i<numbers.length;i++){ //fill each index with a non repetitive random number
            boolean nonrep = false;
            int randomInt1 = r.nextInt(60);
            int randomNext = 0;
            while(nonrep!=true){//while loop to check if random number is repeated
                if(randomInt1!=randomNext){
                    randomNext = randomInt1;
                    nonrep = true;
                    numbers[i] = randomNext;
                }
                else if(randomInt1==randomNext){
                   randomInt1 = r.nextInt(60);
                }
            }

        }

        return numbers;
    }

    public static boolean lotterywin(int[]usernumber, int[]lotterynumber){//method to check if the user entered lottery numbers and the randomly generated numbers match
        boolean result = true;
        for(int i = 0; i<usernumber.length;i++){//check every index of both arrays for similarity
            if(usernumber[i]!=lotterynumber[i]){
                result = false;
            }
        }

        return result;
    }

}
