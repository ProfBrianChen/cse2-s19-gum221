//CSE@2
//Gustav Masch

import java.util.*;
public class hw06 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //initiate scanner for user to enter number
        Scanner myScanner = new Scanner(System.in);
        //prompt user to enter number
        System.out.println("Enter non-negative height integer: ");
        //create boolean to verify if user enterd an integer
        boolean trueheight = myScanner.hasNextInt();
        //ccheck if integer is positive
        while(trueheight != true){
          String junkWord = myScanner.next();
          System.out.println("Enter a valid height integer");
          trueheight = myScanner.hasNextInt();
        }//create a variable to store users number
        int height = myScanner.nextInt();
        
        System.out.println("Enter non-negative width integer: ");
        //create boolean to verify if user enterd an integer
        boolean truewidth = myScanner.hasNextInt();
        //ccheck if integer is positive
        while(truewidth != true){
          String junkWord = myScanner.next();
          System.out.println("Enter a valid width integer");
          truewidth = myScanner.hasNextInt();
        }//create a variable to store users number
        int width = myScanner.nextInt();
        
        System.out.println("Enter non-negative square integer: ");
        //create boolean to verify if user enterd an integer
        boolean truesquare = myScanner.hasNextInt();
        //ccheck if integer is positive
        while(truesquare != true){
          String junkWord = myScanner.next();
          System.out.println("Enter a valid square integer");
          truesquare = myScanner.hasNextInt();
        }//create a variable to store users number
        int square = myScanner.nextInt();
        
        
        System.out.println("Enter non-negative edge integer: ");
        //create boolean to verify if user enterd an integer
        boolean truesize = myScanner.hasNextInt();
        //ccheck if integer is positive
        while(truesize != true){
          String junkWord = myScanner.next();
          System.out.println("Enter a valid size integer");
          truesize = myScanner.hasNextInt();
        }//create a variable to store users number
        int edge = myScanner.nextInt();
        
        int vline = 0;
        int vcount=0;
        int vheight=0;
        int scount=0;
        int dcount=0;
        int space=0;
        int hcount = 0;
        int amount = (width)/(square+edge);
        
        if(square==1){
            while(vheight<height){
                if(vheight==0||vheight%edge==0){
                    while(hcount<=width){
                        if (hcount == 0 || hcount%edge == 0){
                        System.out.print("#");
                        hcount++;
                        }
                        else  {
                        System.out.print("-");
                        hcount++;
                        }
                    }
                    hcount = 0;
                    vheight++;
                }
                else {
                    while(vline<width){
                        if (vline == 0 || vline%edge == 0){
                            System.out.print("|");
                            vline++;
                        }
                        else {
                            System.out.print(" ");
                            vline++;
                        } 
                    }
                    vline = 0;
                }
                System.out.print("\n");
                vheight++;
            }

        }
        else if(square%2!=0){
            while(vheight<height){
                if(vheight==0||vheight%square==0){
                    while(hcount<=width){
                        if (hcount == 0 || hcount%square == 0){
                        System.out.print("#");
                        hcount++;
                        }
                        else  {
                        System.out.print("-");
                        hcount++;
                        }
                    }
                    hcount = 0;
                }
                else {
                    while(vline<width){
                        if (vline == 0 || vline%square == 0){
                            System.out.print("|");
                            vline++;
                        }
                        else {
                            System.out.print(" ");
                            vline++;
                        } 
                    }
                    vline = 0;
                }
                System.out.print("\n");
                vheight++;
            }
            
        }
        else if(square%2==0) {
            for(int i = 0; i < amount; i++){
                    for(int k = 0; k < amount; k++){
                        System.out.print("#");
                        for(int l = 0; l < square; l++){
                            System.out.print("-");
                        }
                        System.out.print("#");
                        for(int m = 0; m < width; m++){
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                    for(int k = 0; k < square; k++){
                        for(int p = 0; p < amount; p++){
                            System.out.print("|");
                            for(int l = 0; l < square; l++){
                                System.out.print(" ");
                            }
                            System.out.print("|");
                            for(int m = 0; m < edge; m++){
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                    for(int k = 0; k < amount; k++){
                        System.out.print("#");
                        for(int l = 0; l < square; l++){
                            System.out.print("-");
                        }
                        System.out.print("#");
                        for(int m = 0; m < edge; m++){
                            System.out.print(" ");
                        }
                    }
                    for(int r = 0; r < edge; r++){
                        for(int t = 0; t < width; t++){
                            if(t == square/2 || t == (square/2 + 1)){
                                System.out.print("|");
                            }
                            else{
                            System.out.print(" ");}
                        }
                        System.out.println();
                    }
                }
        } 
    }
        
}