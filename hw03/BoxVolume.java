//Gustav Masch
//CSE2
//HW03 Pt.2
//In this part of homework 3 we will prompt the user to enter the length, width and height of a box 
//and while using the Scanner declaration and construction statements we will take the entereed values
//to calculate the volume of the box

import java.util.Scanner;

public class BoxVolume{
//main mehtod required for every Java program
  public static void main(String args[]){
    Scanner myScanner = new Scanner ( System.in );
    //Declaration and Construction statements
    System.out.println("The width side of the box is:");
    double width = myScanner.nextDouble();
    //Prompt the user to enter the width of the box and define the width variable with the entered value
    System.out.println("The length of the box is:");
    double length = myScanner.nextDouble();
    //Prompt the user to enter the length of the box and define the length variable with the entered value
    System.out.println("The height of the box is:");
    double height = myScanner.nextDouble();
    //Prompt the user to enter the height of the box and define the height variable with the entered value
    double Volume=height*width*length;
    //Establish the volume variable and calculate the volume of the box with the entered values
    System.out.println("The volume inside the box is: " + Volume);
    //Print the result of the Volume equation to the user
  }
}