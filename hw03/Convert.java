//Gustav Masch
//CSE2
//HW03 Pt.1
//In this part of homework 3 we will take the meters value entered by the user using the
//Scanner declaration and construction statement to extract the value entered by the user and
//then multiply it by the conversion factor found in google and finally print the result.

import java.util.Scanner;

public class Convert{
//main mehtod required for every Java program
  public static void main(String args[]){
    Scanner myScanner = new Scanner ( System.in );
    //Declaration and Construction statements
    System.out.println("Enter the distance in meters: ");
    //Line to prompt the user to enter the distance
    double distance = myScanner.nextDouble();
    //Define the variable distance as the value the user entered
    double conversion = distance*39.37;
    //Convert the value from the user from meter to inches
    System.out.println( distance + " meters is " + conversion + " inches.");
    //Print out the result of the conversion
  }
}