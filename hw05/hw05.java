//HW05
//CSE002
//Gustav Masch
//In this homework we will create a variety of while loops to ask the user to enter certain information while analyzing the data
//in real time and consequently inform the user if their information is wrong and prompt them to enter it again.

import java.util.*;
public class hw05 {
  //main method required for every java programm
    public static void main(String[] args) {
      //initalize the Scanner class for further input of the user in the program
        Scanner myScanner = new Scanner(System.in);
      
      //Prompt user to enter a coursenumber
        System.out.println("Enter a course number"); 
      //Check if the number entered by the user is a valid integer
        while(myScanner.hasNextInt() == false){ 
          //inform user that the number entered is not valid
            System.out.println("Enter a valid course number"); 
            myScanner.next();
        }
      //save the users number
        int coursenumber = myScanner.nextInt();
      
      //prompt the user to enter the name of the department of the course  
        System.out.println("Enter the department name"); 
      //check if infromation entered by the user is valid
        while(myScanner.hasNext() == false){ 
          //prompt the user ot enter a valid entry
            System.out.println("Enter a valid department"); 
            myScanner.next();
        }
      //save the users number
        String depname = myScanner.nextLine();
        myScanner.next();
        
      //prompt user to enter the number of times the corse meets in a week
        System.out.println("Enter a number of times the course meets in a week"); 
      //check if the number entered is a valid integer
        while(myScanner.hasNextInt() == false){ 
          //prompt user to enter a valid number
            System.out.println("Enter a valid number of times the course meets in a week"); 
            myScanner.next();
        }
      //save the users number
        int weekmeets = myScanner.nextInt(); 
      
      //prompt the user to enter the time the course meets  
        System.out.println("Enter the time the class starts in the format hh.mm");
      //check if time is correct
          while(myScanner.hasNextDouble() == false){
            //ask user for a valid number
            System.out.println("Enter a valid time"); 
            myScanner.next();
         }
      //save the users number
        double timestart = myScanner.nextDouble();
      
      //prompt the user to enter the instructor's name  
        System.out.println("Enter the instructor's name");
      //check if infromation entered is valid
        while(myScanner.hasNext() == false){ 
          //Prompt the user to enter a valid name
            System.out.println("Enter a valid department"); 
            myScanner.next();
        }
      //save the users name
        String instname = myScanner.nextLine();
        myScanner.next();
        
      //prompt user to enter the number of students in their class
        System.out.println("Enter the number of students in the class");
      //check if the number entered by the user is a valid integer
        while(myScanner.hasNextInt() == false){ 
          //Prompt the user to enter a valid number
            System.out.println("Enter a valid number of students"); 
            myScanner.next();
        }
      //save the users number
        int nostudents = myScanner.nextInt();
        myScanner.next();
      
      System.out.println("Success!")
    }
}

